/*===========================================================================*\
|*  libunr-OpenGL3Drv - An OpenGL3 Render Device for libunr                  *|
|*  Copyright (C) 2018-2019  Adam W.E. Smith                                 *|
|*                                                                           *|
|*  This program is free software: you can redistribute it and/or modify     *|
|*  it under the terms of the GNU General Public License as published by     *|
|*  the Free Software Foundation, either version 3 of the License, or        *|
|*  (at your option) any later version.                                      *|
|*                                                                           *|
|*  This program is distributed in the hope that it will be useful,          *|
|*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *|
|*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            *|
|*  GNU General Public License for more details.                             *|
|*                                                                           *|
|*  You should have received a copy of the GNU General Public License        *|
|*  along with this program. If not, see <https://www.gnu.org/licenses/>.    *|
\*===========================================================================*/

/*========================================================================
 * OpenGL3Funcs.cpp - OpenGL extensions used by the renderer
 *
 * written by Adam 'Xaleros' Smith
 *========================================================================
*/

#include "OpenGL3RenderDevice.h"
#include "OpenGL3Funcs.h"

bool UOpenGL3RenderDevice::GetExtensions()
{
#if defined LIBUNR_WIN32
  LOAD_EXT_PROC( wglChoosePixelFormatARB, PFNWGLCHOOSEPIXELFORMATARBPROC );
  LOAD_EXT_PROC( wglCreateContextAttribsARB, PFNWGLCREATECONTEXTATTRIBSARBPROC );
#endif

  LOAD_EXT_PROC( glGenBuffers, PFNGLGENBUFFERSPROC );
  LOAD_EXT_PROC( glBindBuffer, PFNGLBINDBUFFERPROC );
  LOAD_EXT_PROC( glBufferData, PFNGLBUFFERDATAPROC );
  LOAD_EXT_PROC( glDeleteBuffers, PFNGLDELETEBUFFERSPROC );

  LOAD_EXT_PROC( glActiveTexture, PFNGLACTIVETEXTUREPROC );

  LOAD_EXT_PROC( glBlendFuncSeparate, PFNGLBLENDFUNCSEPARATEPROC );
  LOAD_EXT_PROC( glBlendEquation, PFNGLBLENDEQUATIONPROC );

  LOAD_EXT_PROC( glGenVertexArrays, PFNGLGENVERTEXARRAYSPROC );
  LOAD_EXT_PROC( glBindVertexArray, PFNGLBINDVERTEXARRAYPROC );
  LOAD_EXT_PROC( glDeleteVertexArrays, PFNGLDELETEVERTEXARRAYSPROC );

  LOAD_EXT_PROC( glDrawRangeElements, PFNGLDRAWRANGEELEMENTSPROC );
  LOAD_EXT_PROC( glPrimitiveRestartIndex, PFNGLPRIMITIVERESTARTINDEXPROC );

  LOAD_EXT_PROC( glCreateShader, PFNGLCREATESHADERPROC );
  LOAD_EXT_PROC( glShaderSource, PFNGLSHADERSOURCEPROC );
  LOAD_EXT_PROC( glCompileShader, PFNGLCOMPILESHADERPROC );
  LOAD_EXT_PROC( glCreateProgram, PFNGLCREATEPROGRAMPROC );
  LOAD_EXT_PROC( glAttachShader, PFNGLATTACHSHADERPROC );
  LOAD_EXT_PROC( glDeleteShader, PFNGLDELETESHADERPROC );
  LOAD_EXT_PROC( glGetShaderiv, PFNGLGETSHADERIVPROC );
  LOAD_EXT_PROC( glGetShaderInfoLog, PFNGLGETSHADERINFOLOGPROC );
  LOAD_EXT_PROC( glLinkProgram, PFNGLLINKPROGRAMPROC );
  LOAD_EXT_PROC( glUseProgram, PFNGLUSEPROGRAMPROC );
  LOAD_EXT_PROC( glDeleteProgram, PFNGLDELETEPROGRAMPROC );
  LOAD_EXT_PROC( glGetProgramiv, PFNGLGETPROGRAMIVPROC );
  LOAD_EXT_PROC( glGetProgramInfoLog, PFNGLGETPROGRAMINFOLOGPROC );

  LOAD_EXT_PROC( glVertexAttribPointer, PFNGLVERTEXATTRIBPOINTERPROC );
  LOAD_EXT_PROC( glEnableVertexAttribArray, PFNGLENABLEVERTEXATTRIBARRAYPROC );
  LOAD_EXT_PROC( glDisableVertexAttribArray, PFNGLDISABLEVERTEXATTRIBARRAYPROC );

  LOAD_EXT_PROC( glUniform1i, PFNGLUNIFORM1IPROC );
  LOAD_EXT_PROC( glUniform3fv, PFNGLUNIFORM3FVPROC );
  LOAD_EXT_PROC( glUniform4f, PFNGLUNIFORM4FPROC );
  LOAD_EXT_PROC( glUniformMatrix4fv, PFNGLUNIFORMMATRIX4FVPROC );
  LOAD_EXT_PROC( glGetUniformLocation, PFNGLGETUNIFORMLOCATIONPROC );

  return true;
}

#if defined LIBUNR_WIN32
PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB = NULL;
PFNWGLCREATECONTEXTATTRIBSARBPROC wglCreateContextAttribsARB = NULL;
#endif

// Buffer extensions
PFNGLGENBUFFERSPROC glGenBuffers = NULL;
PFNGLBINDBUFFERPROC glBindBuffer = NULL;
PFNGLBUFFERDATAPROC glBufferData = NULL;
PFNGLDELETEBUFFERSPROC glDeleteBuffers = NULL;

// Texture extensions
PFNGLACTIVETEXTUREPROC glActiveTexture = NULL;

// Blending extensions
PFNGLBLENDFUNCSEPARATEPROC glBlendFuncSeparate = NULL;
PFNGLBLENDEQUATIONPROC     glBlendEquation = NULL;

// Vertex array extensions
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays = NULL;
PFNGLBINDVERTEXARRAYPROC glBindVertexArray = NULL;
PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays = NULL;

// Drawing extensions
PFNGLDRAWRANGEELEMENTSPROC glDrawRangeElements = NULL;
PFNGLPRIMITIVERESTARTINDEXPROC glPrimitiveRestartIndex = NULL;

// Shader extensions
PFNGLCREATESHADERPROC      glCreateShader = NULL;
PFNGLSHADERSOURCEPROC      glShaderSource = NULL;
PFNGLCOMPILESHADERPROC     glCompileShader = NULL;
PFNGLCREATEPROGRAMPROC     glCreateProgram = NULL;
PFNGLATTACHSHADERPROC      glAttachShader = NULL;
PFNGLDELETESHADERPROC      glDeleteShader = NULL;
PFNGLGETSHADERIVPROC       glGetShaderiv = NULL;
PFNGLGETSHADERINFOLOGPROC  glGetShaderInfoLog = NULL;
PFNGLLINKPROGRAMPROC       glLinkProgram = NULL;
PFNGLUSEPROGRAMPROC        glUseProgram = NULL;
PFNGLDELETEPROGRAMPROC     glDeleteProgram = NULL;
PFNGLGETPROGRAMIVPROC      glGetProgramiv = NULL;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = NULL;

// Vertex attribute extensions
PFNGLVERTEXATTRIBPOINTERPROC     glVertexAttribPointer = NULL;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray = NULL;
PFNGLDISABLEVERTEXATTRIBARRAYPROC glDisableVertexAttribArray = NULL;

// Uniform shader extensions
PFNGLUNIFORM1IPROC glUniform1i = NULL;
PFNGLUNIFORM3FVPROC glUniform3fv = NULL;
PFNGLUNIFORM4FPROC glUniform4f = NULL;
PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv = NULL;
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation = NULL;
